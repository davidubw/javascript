﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WebForm1.aspx.vb" Inherits="CookiesProject.WebForm1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script type="text/javascript" >
    function setCheck() {

        //alert(getCookie("PAGE","PAGEKEY"));
        addToCookie("PAGE", "PAGEKEY", "TESTING200");
    }

    function parseCookie(name) { 
      var str = "" 
      var a = document.cookie.split(";") 
      for (var i=0; i<a.length; i++) { 
        a[i] = a[i].trim() 
        if (a[i].substring(0,name.length) == name) 
          str = a[i].substring(name.length+1) 
      } 
      var b = str.split("&") 
      var obj = new Object() 
      if (str.length) 
        for (i=0; i<b.length; i++) { 
          temp = b[i].split("=") 
          obj[temp[0]] = temp[1] 
        } 
        return obj
    }

    function addToCookie(cookieName,key,value) {
        var cookies = parseCookie(cookieName);
        cookies[key] = value;
        var d = new Date();
        d.setFullYear(d.getFullYear() + 1);
        var cookieStr = cookieName + "=";
        for (var item in cookies);
        cookieStr += item + "=" + cookies[item] + "&";
        cookieStr = cookieStr.substring(0, cookieStr.length - 1);
        cookieStr = cookieStr + "; expires=" + d.toUTCString();
        cookieStr = cookieStr + "; path=/";
        document.cookie = cookieStr; 
  }

  function getCookie(name, key) {
      var cookies = parseCookie(name);
      return cookies[key];
  }

  function checkChanged(s, e) {
      alert("Changed");
  }

    setCheck();
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:CheckBox ID="CheckBox1" runat="server"/>
        <asp:CheckBox ID="CheckBox2" runat="server" />
        <asp:CheckBox ID="CheckBox3" runat="server" />
    
    </div>
    </form>
</body>
</html>
