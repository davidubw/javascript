﻿Imports System.Web.UI.Page

Public Class MyCookie
    Private _page As Page
    Private _pageKey As String = String.Empty
    Public Sub New(ByRef page As Page, ByRef name As String)
        _page = page
        _pageKey = name + "100"
        setCookie("PAGE", "PAGEKEY", _pageKey)
    End Sub

    Public Sub setCookie(ByRef key As String, ByRef subkey As String, ByRef value As String)
        Dim aCookie As New HttpCookie(key)
        aCookie.Values(subkey) = _pageKey
        aCookie.Values(subkey & "2") = "222"
        aCookie.Expires = DateTime.Now.AddDays(1)
        _page.Response.Cookies.Add(aCookie)
    End Sub

End Class
